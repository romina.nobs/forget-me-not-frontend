import React from 'react'
import PropTypes from 'prop-types'
import logo from '../images/logo.svg'
import Button from './Button'
import '../App.css'

const Footer = ({ title }) => {
  const onClick = () => {
    window.open(
      'https://www.figma.com/file/om2WaRBfGS7INPa6jBGEBz/forget-me-not?node-id=0%3A1',
      '_blank'
    )
  }

  return (
    <footer className='header'>
      <img src={logo} className='App-logo' alt='logo' />
      <Button onClick={onClick} text='Design - System' />
      <img src={logo} className='App-logo' alt='logo' />
      <p>Copyright &copy; 2021</p>
      <a href='https://karmacoma.ch/'>Karmacoma Artwork</a>
    </footer>
  )
}
Footer.defaultProps = {
  title: 'App'
}

Footer.propTypes = {
  title: PropTypes.string
}
export default Footer
