import React from 'react'
import PropTypes from 'prop-types'
import logo from '../images/logo.svg'
import '../App.css'

const Header = ({ title }) => {
  return (
    <header className='header'>
      <img src={logo} className='App-logo' alt='logo' />
      <h1>Forget-me-not {title}</h1>
    </header>
  )
}
Header.defaultProps = {
  title: 'App'
}

Header.propTypes = {
  title: PropTypes.string
}
export default Header
