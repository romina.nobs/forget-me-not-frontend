import React from 'react'
import { FaEdit } from 'react-icons/fa'
import PropTypes from 'prop-types'
import '../App.css'

const Reminder = ({ reminder, onClick, editmode }) => {
  return (
    <div className='reminder'>
      <h5>
        {reminder.title}

        {editmode && (
          <FaEdit
            style={{ color: '#6D9EFC', fontSize: '30px', cursour: 'pointer' }}
            onClick={() => onClick(reminder)}
          />
        )}
      </h5>
      <p>
        {reminder.allowNotificationFrom} - {reminder.allowNotificationUntil}
      </p>
      <p>all {reminder.interval} Minutes</p>
    </div>
  )
}

Reminder.propTypes = {
  reminder: PropTypes.object,
  onClick: PropTypes.func,
  editmode: PropTypes.bool
}

export default Reminder
