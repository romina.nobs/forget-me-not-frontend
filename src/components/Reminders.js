import React from 'react'
import Reminder from './Reminder'

const Reminders = ({ reminders, onClick, editmode }) => {
  return (
    <div className='multible-reminders'>
      {reminders.map(reminder => (
        <Reminder
          key={reminder._id}
          reminder={reminder}
          onClick={onClick}
          editmode={editmode}
        />
      ))}
    </div>
  )
}

export default Reminders
